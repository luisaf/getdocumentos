const moment = require('moment-timezone');
const Controller = require('../../../utils/controller');
const Read = require('../models/read');

moment.locale('es');

class controller extends Controller {
	async ultimosEventos(coleccion, formulario, documento, limite) {
		try {
			const modelRead = await new Read();
			let docs = [];

			docs = await modelRead.getDocs(coleccion, documento, limite);

			const respuesta = docs.datos.map(element => {
				return {
					nombre: element.nombre,
					iconoUrl: element.iconoUrl
				};
			});


			return {
				status: docs.status,
				message: 'ok',
				data: respuesta
			};
		} catch (error) {
			console.log(error);
			return error;
		}
	}
}

module.exports = controller;
