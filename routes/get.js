const Joi = require('@hapi/joi');
const Controller = require('../controllers/get');

module.exports = [
	{
		method: 'GET',
		path: '/api/v1/getDocumentos',
		async handler(req) {
			const controller = await new Controller(req);
			const { coleccion} = req.payload;

			return await controller.ultimosEventos(
				coleccion
			);
		},
		options: {
			auth: {
				strategy: 'firebase'
			},
			validate: {
				payload: Joi.object({
					coleccion: Joi.array()
						.min(1)
						.required()
				})
			},
			description: 'Consultar documentos de una coleccion',
			tags: ['api']
		}
	}

];
