const Boom = require('boom');
const Model = require('../../../utils/model');

class model extends Model {
	
	async getDocs(coleccion, documento, limite) {
		const querys = coleccion.map(element => {
			return this.dbFirestore
				.collection(element.toLowerCase())
				.get();
		});

		try {
			const res = await Promise.all(querys);
			const datos = [];

			res.map((element, index) => {
				element.forEach(doc => {
					const document = doc.data();
					document.coleccionOrigen = coleccion[index];
					datos.push(document);
				});
				return 0;
			});
			return { datos, status: 200 };
		} catch (error) {
			console.log(error);
			console.log(Boom.notFound('Error leyendo documentos', error));
			return Boom.notFound('Error leyendo documentos', error);
		}
	}
}

module.exports = model;
